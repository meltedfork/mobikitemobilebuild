/**
 * @author Steven M. Redman
 */

var app = angular.module('app', ['ngRoute']);

app.config(['$routeProvider', function($routeProvider) {
	$routeProvider.
		when('/', {
			templateUrl: 'signIn.html',
			controller: 'SignInController'
		}).
		when('/app', {
			templateUrl: 'app.html',
			controller: 'AppController'
		}).
		when('/scan', {
			templateUrl: 'scan.html',
			controller: 'SignInScannerController'
		}).
		otherwise({
			redirectTo: 'signIn.html'
		});
}]);

app.service('AppService', function($http) {
	var user = {
		email: null,
		password: null,
		businessName: "",
		businessId: 0,
	//	pin: null,
		logo: 'res/logo.png'
	};

	this.signIn = function(u, callback) {
		GLOBAL.showProgress();
		var postInit = GLOBAL.initPost(u, 'authenticate');
		$http(postInit).
			success(function(data, status, headers, config) {
				GLOBAL.hideProgress();
				if (data.error == false) {
					user.email = u.email;
					user.password = u.password;
					user.businessName = data.businessName;
					user.businessId = data.businessId;
			//		user.pin = u.pin;
					if (data.logo != null) user.logo = data.logo;
				}
				callback(data.error);
			}).
			error(function(data, status, headers, config) {
				GLOBAL.hideProgress();
				GLOBAL.showAlert(GLOBAL.errors.NETWORK);
			});
	};

	this.createCustomer = function(customer, callback) {
		GLOBAL.showProgress();
		var postInit = GLOBAL.initPost({customer: customer, user: user}, 'customer/create');
		$http(postInit).
			success(function(data, status, headers, config) {
				GLOBAL.hideProgress();
				callback(data.error);
			}).
			error(function(data, status, headers, config) {
				GLOBAL.hideProgress();
				GLOBAL.showAlert(GLOBAL.errors.NETWORK);
			});
	};

	this.getUser = function() {
		return user;
	};

	this.resetUser = function() {
		user.email = null;
		user.password = null;
		user.businessName = "";
		user.businessId = 0;
	//	user.pin = null;
		user.logo = 'res/logo.png';
	};

	this.getNewCustomerModel = function() {
		return {
			firstName: "",
			lastName: "",
			email: "",
			areaCode: null,
			phoneFirst: null,
			phoneLast: null,
			contactEmail: 1,
			contactSMS: 1
		};
	};
});

app.controller('SignInController', ['$scope', '$location', 'AppService', function($scope, $location, AppService) {
	$scope.submitted = false;

	var user = AppService.getUser();
	if (user.email != null) $location.path('/app');

	$scope.signIn = function() {
		$scope.submitted = true;
		if ($scope.form.$valid) {
			AppService.signIn($scope.user, function(error) {
				if (error == false) {
					$location.path('/app');
				}
				else {
					GLOBAL.showAlert("Your credentials are invalid.");
				}
			});
		}
	};

	$scope.signInScanner = function() {
		$scope.submitted = true;
		if ($scope.form.$valid) {
			AppService.signIn($scope.user, function(error) {
      	if (error == false) {
					$location.path('/scan');
				} else {
					GLOBAL.showAlert("Your credentials are invalid.");
				}
			});
		}
	};

}]);

app.controller('SignInScannerController', ['$scope', function($scope) {
	$scope.scan = function() {
	      cordova.plugins.barcodeScanner.scan(
	        function (result) {
	          if(!result.cancelled) {
	            if(result.format == "QR_CODE") {
	              var value = result.text;
	              var data = JSON.parse(value);
	              window.open(data, '_blank', 'location=yes');
	            }
	          }
	        },
	        function (error) {
	          alert("Scanning failed: " + error);
	        }
	      );
	    }

}]);

app.controller('AppController', ['$scope', '$location', 'AppService', function($scope, $location, AppService) {
	$scope.user = AppService.getUser();
	console.log($scope.user);
	$scope.customer = AppService.getNewCustomerModel();
	$scope.submitted = false;
	$scope.signOutSubmitted = false;
	if ($scope.user.email == null) $location.path('/');

	$scope.createCustomer = function() {
		$scope.submitted = true;
		if ($scope.appForm.$valid) {
			AppService.createCustomer($scope.customer, function(error) {
				if (error) {
					GLOBAL.showAlert("Sorry, but there was problem while processing your request.");
				}
				else {
					GLOBAL.showAlert("Thank you for signing up! You will receive a confirmation email and/or text message shortly. You will also be notified when new mobile reward programs are available.");
					$scope.cancel();
				}
			});
		}
	};

	$scope.cancel = function() {
		$scope.customer = AppService.getNewCustomerModel();
		$scope.submitted = false;
		$scope.appForm.$setPristine();
	};

	$scope.signOut = function() {
		$('#SignOutModal').modal();
	};

	$scope.confirmSignOut = function() {
		$scope.signOutSubmitted = true;
		if ($scope.signOut.pin == $scope.user.pin) {
                                 // Show splash screen (useful if your app takes time to load)
                                 navigator.splashscreen.show();
                                 // Reload original app url (ie your index.html file)
                                 window.location = initialHref;

//			$('#SignOutModal').modal("hide");
//			$scope.user = null;
//            AppService.resetUser();
//			$location.path('/');

         }
	};
}]);

app.directive('focusNext', function() {
	return {
		restrict: 'A',
		link: function(scope, elem, attrs, ctrl) {
			$(elem).keyup(function() {
				var val = $(this).val().trim();
				if (val.length == attrs.maxlength) {
					$(attrs.focusNext).focus();
				}
			});
		}
	};
});
