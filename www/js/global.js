/**
 * @author Steven M. Redman
 */

var GLOBAL = (function() {
	this.domain = "https://mymobiledashboard.com/api/";
	
	this.initPost = function(obj, path) {
		return {
			url: this.domain + path,
			method: 'POST',
			data: (obj != null ? $.param(obj) : null),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
	};
	
	this.showAlert = function(text) {
		$('#AlertMsg').html(text);
		$('#AlertModal').modal();
	};
	
	this.showProgress = function() {
		$('#MainProgress').show();
	};
	
	this.hideProgress = function() {
		$('#MainProgress').hide();
	};
	
	this.errors = {
		NETWORK: "A network error occurred while processing your request."
	};
	
	return this;
}());
